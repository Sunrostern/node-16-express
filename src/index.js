'use strict';

const app = require('express')();

const port = process.env.PORT || 3000;

app.get('/', (request, response) => {
  response.json({
    message: "Hello, world!"
  });
});

app.listen(port, () => {
  console.log(`App listening at Port ${port}.`);
});
